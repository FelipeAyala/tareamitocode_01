package com.softpya.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.softpya.model.Producto;
import com.softpya.service.IProductoService; 

@RestController
@RequestMapping("/productos")
public class ProductoController {
	
	@Autowired
	private IProductoService service; 
	
	@GetMapping(consumes="application/json", produces="application/json")
	public List<Producto> listar(){
		return service.listar();
	}
	
	@GetMapping(value="/{id}")
	public Producto listarPorId(@PathVariable ("id") Integer id)
	{
		return service.listarPorId(id);
	}
	
	@PostMapping(consumes="application/json", produces="application/json")
	public Producto registrar(@RequestBody Producto Producto)
	{
		return service.registrar(Producto);
	}
	
	@DeleteMapping(value="/{id}")
	public void eliminar(@PathVariable("id") Integer id)
	{
		service.eliminar(id);
	}
	
	@PutMapping(consumes="application/json", produces="application/json")
	public Producto modificar(@RequestBody Producto Producto)
	{
		return service.modificar(Producto);
	}
}
