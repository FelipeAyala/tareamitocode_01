package com.softpya.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.softpya.model.Persona;

public interface PersonaDAO extends JpaRepository<Persona, Integer>  {
	 
}
