package com.softpya.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.softpya.model.Producto;

public interface ProductoDAO extends JpaRepository<Producto, Integer> {
	 
}
