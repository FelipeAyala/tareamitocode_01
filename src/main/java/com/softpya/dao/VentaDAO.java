package com.softpya.dao;

import org.springframework.data.jpa.repository.JpaRepository;
 
import com.softpya.model.Venta;

public interface VentaDAO extends JpaRepository<Venta, Integer>  {
	 
}
