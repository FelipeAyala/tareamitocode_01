package com.softpya.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.softpya.dao.PersonaDAO;
import com.softpya.model.Persona;
import com.softpya.service.IPersonaService; 

@Service
public class PersonaServiceImpl implements IPersonaService {

	@Autowired
	private PersonaDAO dao; 

	@Override
	public List<Persona> listar(){
		return dao.findAll();
	}
	
	@Override
	public Persona listarPorId(int id)
	{
		return dao.findOne(id);
	}
	
	@Override
	public Persona registrar(Persona persona)
	{
		return dao.save(persona);
	}
	
	@Override
	public void eliminar(int idPersona)
	{
		dao.delete(idPersona);
	}
	
	@Override
	public Persona modificar(Persona persona)
	{
		return dao.save(persona);
	}
	
}
