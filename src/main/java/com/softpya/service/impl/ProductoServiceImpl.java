package com.softpya.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.softpya.dao.ProductoDAO; 
import com.softpya.model.Producto;
import com.softpya.service.IProductoService;

@Service
public class ProductoServiceImpl implements IProductoService {

	@Autowired
	private ProductoDAO dao; 

	@Override
	public List<Producto> listar(){
		return dao.findAll();
	}
	
	@Override
	public Producto listarPorId(int id)
	{
		return dao.findOne(id);
	}
	
	@Override
	public Producto registrar(Producto Producto)
	{
		return dao.save(Producto);
	}
	
	@Override
	public void eliminar(int idProducto)
	{
		dao.delete(idProducto);
	}
	
	@Override
	public Producto modificar(Producto Producto)
	{
		return dao.save(Producto);
	}

}
