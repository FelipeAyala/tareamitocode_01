package com.softpya.service.impl;

import java.util.List;
  
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.softpya.dao.VentaDAO;
import com.softpya.model.DetalleVenta;
import com.softpya.model.Venta;
import com.softpya.service.IVentaService;

@Service
public class VentaServiceImpl implements IVentaService {

	@Autowired
	private VentaDAO dao; 

	@Override
	public List<Venta> listar(){
		return dao.findAll();
	}
	
	@Override
	public Venta listarPorId(int id)
	{
		return dao.findOne(id);
	}
	
	@Override
	public Venta registrar(Venta venta)
	{
		for(DetalleVenta det : venta.getDetalleVenta()) {
			det.setVenta(venta);
		}
		return dao.save(venta);
	} 
	
	@Override
	public void eliminar(int idVenta)
	{
		dao.delete(idVenta);
	}
	
	@Override
	public Venta modificar(Venta Venta)
	{
		return dao.save(Venta);
	}

}
